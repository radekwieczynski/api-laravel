<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();

        $faker = \Faker\Factory::create();

        $password = \Illuminate\Support\Facades\Hash::make('haslo');

        \App\User::create([
        	'name' => 'Administrator',
			'email' => 'admin@test.pl',
			'password' => $password
		]);

        for ($i = 0; $i < 5; $i++) {
        	\App\User::create([
        		'name' => $faker->name,
				'email' => $faker->email,
				'password' => $faker->password
			]);
		}
    }
}
