<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Comments::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
        	\App\Comments::create([
        		'id_parent' => 0,
        		'body' => $faker->sentence
			]);
		}
    }
}
