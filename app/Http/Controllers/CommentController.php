<?php

namespace App\Http\Controllers;

use App\Comments;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
	{
		return Comments::all();
	}

	public function show( $comments)
	{
		return Comments::all()->where('id_parent', $comments);
	}

	public function store(Request $request)
	{
		$comments = Comments::create($request->all());

		return response()->json($comments, 201);
	}

	public function update(Request $request, Comments $comments)
	{
		$comments->update($request->all());

		return response()->json($comments, 200);
	}

	public function delete(Comments $comments)
	{
		$comments->delete();

		return response()->json(null, 204);
	}
}
